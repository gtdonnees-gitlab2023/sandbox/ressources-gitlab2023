### Liens en vrac:

- Page accueil: https://gtdonnees-gitlab2023.sciencesconf.org/
- Programme: https://gtdonnees-gitlab2023.sciencesconf.org/resource/page/id/1

### Gitlab Communautaire vs Payant:

- https://about.gitlab.com/pricing/premium/
- https://about.gitlab.com/pricing/
- https://about.gitlab.com/platform/?stage=plan

### Gitlab: installation

- https://about.gitlab.com/install/

### Gitlab: Documentation(s)

- https://docs.gitlab.com/


